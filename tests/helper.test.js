const chai = require("chai");
const expect = chai.expect; 
chai.use(require("chai-sorted"));
import { sorting, compare, average } from '../src/utils/helper';


test('Should sort the array in ascending order', () => {
	const arr = [5,4,3,2,1];
	const arrSorted = [1,2,3,4,5];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})


let sensors = [
	{
		'SiteName': 'A',
		'PM2.5': "30"
	},
	{
		'SiteName': 'B',
		'PM2.5': "20"
	},
	{
		'SiteName': 'C',
		'PM2.5': "10"
	},
	{
		'SiteName': 'D',
		'PM2.5': "100"
	},
	{
		'SiteName': 'E',
		'PM2.5': "9"
	}

	
];

test('Should sort the sensors by name in ascending order', () => {
	let sortedSensors = sensors.sort(compare);
	var flag = true;
	for (var i = 0; i < sortedSensors.length - 1; i++) {
		if (parseInt(sortedSensors[i]["PM2.5"]) > parseInt(sortedSensors[i+1]["PM2.5"])) {
			flag = false;
			break;
		}
		
	}
	expect(flag).to.equal(true);
	// expect(sortedSensors).to.be.sortedBy("PM2.5", {descending: false});

})


test('Should return average of an array', () => {
	const avg = average([1,2,3]);
	expect(avg).to.equal(2);
})


test('Should round to 2 decimal places', () => {
	const avg = average([12.546, 24.3235, 15.7345]);
	expect(avg).to.equal(17.53);
})