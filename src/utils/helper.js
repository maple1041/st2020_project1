
let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
	return parseInt(a["PM2.5"]) > parseInt(b["PM2.5"]) ? 1 : -1;
}

let average = (nums) => {
	var sum = nums.reduce((previous, current) => current += previous);
	return Math.round(sum / nums.length * 100) / 100;
}


module.exports = {
    sorting,
    compare,
    average
}