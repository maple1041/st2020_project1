# [Project 1 - Gitlab CI, pipeline & unit test](https://hackmd.io/@xlYUTygoRkyuQQlwXuWDWQ/ByaSzDCNL)


Student ID: r08921a26



## Part 1: Unit test (30%)

1. Implement `src/utils/helper.js`.


The funtion of checking sorted PM2.5 in helper.test.js is wrong, 

since it sort the array by string, and it will be wrong if the testcase

has number of PM2.5 over 100 or less then 10.

Therefore, I modified the funtion and add some testcase in it.


2. [Bonus] Test lodash, write your test cases in the `tests/lodash.test.js`.

## Part 2: Pipeline (40%)

1. Add a job called `test function` in test stage parallelly.
2. Add cache to speed up pipeline

## Part 3: Deployment (30%)

* Staging: https://st2020-YOUR_STUDENT_ID-staging.herokuapp.com/
* Prodection: https://st2020-YOUR_STUDENT_ID.herokuapp.com/
